/**
 * 
 */
package hp.simulation.queueset;

import java.lang.reflect.Array;

/**
 * @author Robin Grimsmann
 * @param <E>
 *
 */
public class QueueSetByArray<E> implements QueueSet<E> {
	private E[] objs = new E[16];
	private int cnt = 0;
	private int maxCnt = 16;

	@Override
	public void enqueue(E element) {
		insert(element);
	}

	@Override
	public E dequeue() throws EmptyQueueSetException {
		if (isEmpty()) {
			throw new EmptyQueueSetException("Queue does not contain any elements. Call of dequeue() is illegal.");
		}
		Object tmp = first();
		remove(tmp);
		return tmp;
	}

	@Override
	public E first() throws EmptyQueueSetException {
		if (isEmpty()) {
			throw new EmptyQueueSetException("Queue does not contain any elements. Call of dequeue() is illegal.");
		}
		return objs[0];
	}

	@Override
	public void insert(E element) {
		if (!contains(element)) {
			if (cnt == objs.length) {
				E[] cpy =(E[]) Array.newInstance(E, maxCnt);
				System.arraycopy(objs, 0, cpy, 0, objs.length);
				objs = cpy;
				maxCnt *= 2;
			}
			objs[cnt] = element;
			cnt++;
		}
	}

	@Override
	public void remove(E element) {
		int i = 0;
		boolean found = false;
		for (; i < cnt; i++) {
			if (objs[i] != null) {
				if(objs[i].equals(element)){
                    found = true;
					break;
                }
			} else if(element == null) {
				found = false;
				break;
			}
		}
		for(; i < cnt - 1; i++) {
			objs[i] = objs[i + 1];
		}
		if(found) 
			cnt--;

	}

	@Override
	public boolean contains(E order) {
		for (int i = 0; i < cnt; i++) {
			if (objs[i] != null) {
				if (objs[i].equals(order))
					return true;
			} else if (order == null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public E get(int i) throws QueueSetIndexOutOfBoundsException {
		if (i >= cnt)
			throw new QueueSetIndexOutOfBoundsException(i);
		if (i <= cnt && !isEmpty()) {
			return objs[i];
		}
		return null;
	}

	@Override
	public int size() {
		return cnt;
	}

	@Override
	public boolean isEmpty() {
		return cnt == 0;
	}

	@Override
	public void clear() {
		cnt = 0;
		objs = new E[16];
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < cnt; i++) {
			s += objs[i] + " ";
		}
		return s;
	}

}