/**
 * 
 */
package hp.simulation.queueset;

import java.util.Arrays;

/**
 * @author Robin Grimsmann
 *
 */
public class QueueSetByArray implements QueueSet {
	private Object[] objs = new Object[16];
	private int cnt = 0;
	private int maxCnt = 16;

	@Override
	public void enqueue(Object element) {
		if (cnt >= maxCnt - 1 ) {
			Object[] cpy = new Object[maxCnt * 2];
			System.arraycopy(objs, 0, cpy, 0, objs.length);
			objs = cpy;
			maxCnt *= 2;
		}
		insert(element);
	}

	@Override
	public Object dequeue() throws EmptyQueueSetException {
		if (isEmpty()) {
			throw new EmptyQueueSetException("Queue does not contain any elements. Call of dequeue() is illegal.");
		}
		Object tmp = first();
		remove(tmp);
		return tmp;
	}

	@Override
	public Object first() throws EmptyQueueSetException {
		if (isEmpty()) {
			throw new EmptyQueueSetException("Queue does not contain any elements. Call of dequeue() is illegal.");
		}
		return objs[0];
	}

	@Override
	public void insert(Object element) {
		if (!contains(element)) {
			objs[cnt] = element;
			cnt++;
		}
	}

	@Override
	public void remove(Object element) {
		int hit = -1;
		for (int i = 0; i < cnt; i++) {
			if (objs[i].equals(element)) {
				hit = i;
				objs[i] = null;
			}

			if (hit > -1) {
				objs[i] = objs[i + 1];
			}
		}
		cnt--;

	}

	@Override
	public boolean contains(Object order) {
		for (int i = 0; i < cnt; i++) {
			if (objs[i] != null) {
				if (objs[i].equals(order))
					return true;
			} else if (order == null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Object get(int i) throws QueueSetIndexOutOfBoundsException {
		if (i >= cnt)
			throw new QueueSetIndexOutOfBoundsException(i);
		if (i <= cnt && !isEmpty()) {
			return objs[i];
		}
		return null;
	}

	@Override
	public int size() {
		return cnt;
	}

	@Override
	public boolean isEmpty() {
		return cnt == 0;
	}

	@Override
	public void clear() {
		cnt = 0;
		objs = new Object[16];
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < cnt; i++) {
			s += objs[i] + " ";
		}
		return s;
	}

}